da-lhe: start setup bins dotfiles completions directories finished

files: start bins dotfiles completions directories finished

start:
	@echo "Science, Bitch! Let's installs and configure a lot of stuff..."

setup:
	@echo "Installing..."
	@chmod +x installs.sh
	@./installs.sh

bins:
	@mkdir -p ~/.bin
	@cp -v bin/* ~/.bin
	@chmod +x ~/.bin/*

dotfiles:
	@cp -v -R rcs/ ~/

completions:
	@mkdir -p ~/.completions
	@cp -v completions/* ~/.completions/

directories:
	@mkdir -p ~/Projects/marcospereira
	@mkdir -p ~/Projects/github
	@mkdir -p ~/Projects/bitbucket
	@mkdir -p ~/Programming
	@mkdir -p ~/.gopath

finished:
	@echo "Done!"
	@afplay /System/Library/Sounds/Glass.aiff