#!/usr/bin/env bash

source installs/brew
source installs/git
source installs/python
source installs/rvm
source installs/ruby
source installs/go
source installs/scala
source installs/tools
source installs/gnutar
source installs/direnv

# VirtualBox is a requirement for both k8s and vagrant
source installs/virtualbox 
source installs/kubernetes
source installs/vagrant

source installs/travis
source installs/dmgs
source installs/cleanup
