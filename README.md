# dotfiles

Run `make da-lhe` to install everything.

## Other configurations

- <https://coderwall.com/p/h6yfda/use-and-to-jump-forwards-backwards-words-in-iterm-2-on-os-x>
- <https://coderwall.com/p/ds2dha/word-line-deletion-and-navigation-shortcuts-in-iterm2>
