export PATH=/usr/local/bin:/usr/local/sbin:$PATH

# Local vars
[[ -s "$HOME/.local-env" ]] && source "$HOME/.local-env"

# Load RVM profile
[[ -s "$HOME/.profile" ]] && source "$HOME/.profile"

# Aliases
[[ -s "$HOME/.aliases" ]] && source "$HOME/.aliases"

# Variables and exports
[[ -s "$HOME/.variables" ]] && source "$HOME/.variables"

# Completions
[[ -s "$HOME/.completions/fab_completion" ]] && source "$HOME/.completions/fab_completion"

# External tools
[[ -s "/usr/local/bin/virtualenvwrapper.sh" ]] && source "/usr/local/bin/virtualenvwrapper.sh"
[[ -s "/usr/local/etc/bash_completion.d/scala" ]] && source "/usr/local/etc/bash_completion.d/scala"
[[ -s "/usr/local/etc/bash_completion.d/git-completion.bash" ]] && source "/usr/local/etc/bash_completion.d/git-completion.bash"
[[ -s "/usr/local/etc/bash_completion.d/git-prompt.sh" ]] && source "/usr/local/etc/bash_completion.d/git-prompt.sh"
[[ -s "/usr/local/etc/bash_completion.d/git-extras" ]] && source "/usr/local/etc/bash_completion.d/git-extras"

# In order for gpg to find gpg-agent, gpg-agent must be running, and there must be an env
# variable pointing GPG to the gpg-agent socket. This little script, which must be sourced
# in your shell's init script (ie, .bash_profile, .zshrc, whatever), will either start
# gpg-agent or set up the GPG_AGENT_INFO variable if it's already running.
#
# Credits: https://gist.github.com/bmhatfield/cc21ec0a3a2df963bffa3c1f884b676b
#
if [ -f ~/.gnupg/.gpg-agent-info ] && [ -n "$(pgrep gpg-agent)" ]; then
    source ~/.gnupg/.gpg-agent-info
    export GPG_AGENT_INFO
else
    eval $(gpg-agent --daemon --write-env-file ~/.gnupg/.gpg-agent-info)
fi

# direnv (requires install/direnv)
eval "$(direnv hook bash)"